package entity;

import hxd.Rand;
import h3d.Vector;
import hxd.Key;
import hxd.res.Model;
import h3d.prim.ModelCache;
import h3d.anim.Animation;
import h3d.scene.Object;

class Cube extends Object {
	private var animations:Map<CubeState, Animation> = [];

	private var rand:Rand = Rand.create();
	private var currentIdleTimeSec:Float = 0;
	private var idleTimeSec:Float = 0;

	private var state:CubeState = None;

	private var currentTranslationSpeed = 0.01;
	private var currentRotationSpeed = 1.0;

	private var forward:Int = Key.UP;
	private var jump:Int = Key.SPACE;
	private var left:Int = Key.LEFT;
	private var right:Int = Key.RIGHT;

	public function new(cache:ModelCache, model:Model) {
		super();

		var baseModel = cache.loadModel(model);

		loadAnimation(cache, model, Walk);
		loadAnimation(cache, model, Jump);
		loadAnimation(cache, model, Idle);

		addChild(baseModel);
	}

	function loadAnimation(cache:ModelCache, model:Model, animationName:CubeState) {
		var animation:Animation = cache.loadAnimation(model, animationName.getName());
		animations.set(animationName, animation);
	}

	public function update(dt:Float) {
		if (Key.isPressed(jump)) {
			stopAnimation(true);
			state = Jump;
			if (Key.isDown(forward)) {
				setupMovementAnimation(state);
			} else {
				playAnimation(animations[Jump]).onAnimEnd = resetIdleAnimation;
			}
			resetIdleTimer();
		}

		if (Key.isDown(left)) {
			if (state == Walk) {
				stopAnimation(true);
			}
			rotate(0, 0, -currentRotationSpeed * dt);
			resetIdleTimer();
		} else if (Key.isDown(right)) {
			if (state == Walk) {
				stopAnimation(true);
			}
			rotate(0, 0, currentRotationSpeed * dt);
			resetIdleTimer();
		} else if ((Key.isReleased(right) || Key.isReleased(left)) && Key.isDown(forward) && state == Walk) {
			setupMovementAnimation(state);
		}

		if (Key.isDown(forward) && state != Jump) {
			if (state != Walk) {
				state = Walk;
				setupMovementAnimation(state);
			}
			resetIdleTimer();
		} else if (Key.isReleased(forward)) {
			stopAnimation(true);
			state = None;
			resetIdleAnimation();
		}

		if (state == None) {
			currentIdleTimeSec += dt;
			if (currentIdleTimeSec >= idleTimeSec) {
				state = Idle;
				playAnimation(animations[Idle]).onAnimEnd = resetIdleAnimation;
			}
		}
	}

	function setupMovementAnimation(_state:CubeState) {
		var animation = playAnimation(animations[_state]);
		animation.onAnimEnd = resetIdleAnimation;
		animation.onEvent = moveModel;
		for (i in 0...animation.frameCount) {
			animation.addEvent(i, "" + i);
		}
	}

	function moveModel(data:String) {
		var currentPosition:Vector = getTransform().getPosition();
		var direction:Vector = getTransform().getDirection();
		direction.normalize();
		setPosition(currentPosition.x + (direction.x * currentTranslationSpeed), currentPosition.y + (direction.y * currentTranslationSpeed),
			currentPosition.z);
	}

	function resetIdleTimer() {
		currentIdleTimeSec = 0;
		idleTimeSec = 10 + (10 * rand.rand());
	}

	function resetIdleAnimation() {
		state = None;
		stopAnimation(true);
		resetIdleTimer();
	}
}

enum CubeState {
	None;
	Walk;
	Jump;
	Idle;
}
