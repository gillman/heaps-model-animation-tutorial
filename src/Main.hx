import entity.Cube;

class Main extends hxd.App {
	private var cube:Cube;

	override function init() {
		s3d.lightSystem.ambientLight.set(0.74, 0.74, 0.74);

		var cache = new h3d.prim.ModelCache();
		cache.loadLibrary(hxd.Res.cube);

		cube = new Cube(cache, hxd.Res.cube);
		s3d.addChild(cube);

		cache.dispose();

		new h3d.scene.CameraController(s3d).loadFromCamera();
	}

	override function update(dt:Float) {
		super.update(dt);

		cube.update(dt);
	}

	static function main() {
		hxd.Res.initEmbed();

		new Main();
	}
}
